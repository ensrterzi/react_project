function homeInfosReducer(state, action) {
  switch (action.type) {
    case 'hover_true': {
        return state.map(i=> {
            if(i.id === action.val) {
                i.hover = true;
                return i
            } else {
                return i
            }
        })
    }
    case 'hover_false': {
        return state.map(i=> {
            if(i.id === action.val) {
                i.hover = false;
                return i
            } else {
                return i
            }
        })
    }
    default:
        return state;
  }
}
export default homeInfosReducer