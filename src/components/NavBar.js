import React from 'react'

import { Link } from "react-router-dom";

function NavBar() {
  return (
    <div>
      <nav className="navbar fixed-top navbar-expand-lg navbar-dark">
        <Link className="navbar-brand" to="home">Navbar</Link>

        <button className="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
          <span className="navbar-toggler-icon"></span>
        </button>

        <div className="collapse navbar-collapse" id="navbarNav">
          <ul className="navbar-nav ml-auto">
            <li className="nav-item active">
              <Link to="home">Home</Link>
            </li>
            <li className="nav-item">
              <Link to="about">About</Link>
            </li>
            <li className="nav-item">
              <Link to="contact">Contact</Link>
            </li>
          </ul>

        </div>
      </nav>
        


      
    </div>

  )
}

export default NavBar