import React from 'react'
import { Link } from 'react-router-dom'

function Footer() {
  return (
    <footer className="footer">
      <div className="container">
        <div className="row pt-3">
          <div className="col-md-4 footer-colon">
            <ul>
              <li><Link to="/about">Hakkımızda</Link></li>
              <li><Link to="/contact">İletişim</Link></li>
              <li><Link to="/about">Hakkımızda</Link></li>
              <li><Link to="/contact">İletişim</Link></li>
            </ul>
          </div>
          <div className="col-md-4 footer-colon">
            <ul>
              <li><Link to="/about">Hakkımızda</Link></li>
              <li><Link to="/contact">İletişim</Link></li>
            </ul>
          </div>
          <div className="col-md-4 footer-colon">
            <ul>
              <li><Link to="/about">Sık Sorulan Sorular</Link></li>
              <li><Link to="/contact">Güvenilirlik</Link></li>
            </ul>
          </div>
        </div>
      </div>
    </footer>
  )
}

export default Footer