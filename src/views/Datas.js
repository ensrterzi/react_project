export const datas = [
    {
        id: 1,
        head: "Başlık1",
        description: "İçerik hakkında bilgi",
        image: "duvar1.jpg",
        hover: false
    },
    {
        id: 2,
        head: "Başlık2",
        description: "İçerik hakkında bilgi",
        image: "duvar2.jpg",
        hover: false
    },
    {
        id: 3,
        head: "Başlık3",
        description: "3. İçerik hakkında bilgi",
        image: "duvar1.jpg",
        hover: false
    }
]