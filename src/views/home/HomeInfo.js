import React, { useEffect, useReducer } from 'react'
import homeInfosReducer from'../../reducer/homeınfosReducer'
import { useNavigate } from 'react-router-dom';
import { datas } from "../Datas.js";
function HomeInfo() {
    const navigate = useNavigate();
    const [homeInfos, dispatch] = useReducer(homeInfosReducer, datas)
    const MouseOver = (val) => {
        dispatch({ type: 'hover_true', val: val.id })
    }
    const MouseOut = (val) => {
        dispatch({ type: 'hover_false', val: val.id })
    }
    const infoClick = (item) => {
        console.log('item: ', item)
        navigate(`/homeInfoDetail/${item.id.toString()}`);
    }
    useEffect(() => {
        const data = datas.find(i => i.hover == true)
        if(data) {
            dispatch({ type: 'hover_false', val: data.id })
        }
    }, [])
    
    return (
        <div>
            <div className="home-info container py-5">
                {
                    homeInfos ? homeInfos.map(item => (
                        <div className="row py-5" key={item.id}>
                            <div className={`col-md-6 garden-wall ${item.hover ? "img-trans" : ""}`}  onClick={() => infoClick(item)} style={{ backgroundImage: `url("/img/${item.image}")` }} onMouseOver = {() => MouseOver(item)} onMouseOut={() => MouseOut(item) }>
                                {
                                    item.hover ?
                                        <div className="d-flex justify-content-md-center">
                                            <button type="button">İncele</button>
                                        </div>
                                    : null
                                }
                            </div>
                            <div className="col-md-6 px-5">
                                <h3>{ item.head }</h3>
                                <p>{ item.description }</p>
                                <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Laudantium amet obcaecati dignissimos, quod sit minus natus corrupti nemo quos nihil similique autem provident, placeat, ea est voluptates consectetur optio excepturi tempora laborum inventore doloribus! Officiis, beatae rem et quam omnis ex temporibus qui autem, in corporis explicabo voluptatibus molestias debitis fugiat, veritatis itaque! Reprehenderit impedit quo sed eius ipsum eum voluptas officia omnis vitae obcaecati sint minima rem animi odit, quisquam necessitatibus et debitis repellat atque, beatae, laborum doloribus. Pariatur, quam non possimus qui, repellat consequuntur asperiores ut fuga odio nulla consectetur ullam quia corrupti voluptatum obcaecati deserunt dolorum totam.</p>
                            </div>
                        </div>
                    )) : null
                }
            </div>
        </div>
    )
}

export default HomeInfo