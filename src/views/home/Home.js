import React from 'react'
import HomeInfo from './HomeInfo'

function Home() {
  return (
    <div className="home">
        <div id="carouselExampleCaptions" className="carousel slide my-carousel" data-ride="carousel">

          <ol className="carousel-indicators">
            <li data-target="#carouselExampleCaptions" data-slide-to="0" className="active"></li>
            <li data-target="#carouselExampleCaptions" data-slide-to="1"></li>
          </ol>
          
          <div className="carousel-inner text-center carousel-head">
            <div className="carousel-item active carousel-image bg-img-1">
              {/* <img src="img/bahce1.jpg" alt="" className='d-block w-100'/> */}

              <div className="carousel-caption d-none d-md-block">
                <h5>First slide label</h5>
                <p>Some representative placeholder content for the first slide.</p>
              </div>

            </div>
            <div className="carousel-item carousel-image bg-img-2">

              <div className="carousel-caption d-none d-md-block">
                <h5>Second slide label</h5>
                <p>Some representative placeholder content for the second slide.</p>
              </div>

            </div>
            
          </div>
          <button className="carousel-control-prev" type="button" data-target="#carouselExampleCaptions" data-slide="prev">
            <span className="carousel-control-prev-icon" aria-hidden="true"></span>
            <span className="sr-only">Previous</span>
          </button>
          <button className="carousel-control-next" type="button" data-target="#carouselExampleCaptions" data-slide="next">
            <span className="carousel-control-next-icon" aria-hidden="true"></span>
            <span className="sr-only">Next</span>
          </button>
        </div>

        <HomeInfo />
        <div className="jumbotron jumbotron-fluid bg-dark mb-0">
          <div className="container text-white">
            <h1 className="display-4">Fluid jumbotron</h1>
            <p className="lead">This is a modified jumbotron that occupies the entire horizontal space of its parent.</p>
          </div>
        </div>

    </div>
  )
}

export default Home