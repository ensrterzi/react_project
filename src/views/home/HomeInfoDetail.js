import React, { useEffect, useState } from 'react'
import { useParams } from 'react-router-dom';
import { datas } from "../Datas.js";
function HomeInfoDetail() {
    let { id } = useParams();
    const [activeData, setActiveData] = useState(null);
    useEffect(() => {
      runSearch()  
    }, []);
    const runSearch = () => {
        setActiveData(datas.find(item => item.id == id))
    }
    return (
        <div className="info-detail">
            <h1>{ activeData != null ? activeData.head : null }</h1>
            <p>{ activeData != null ? activeData.description : null }</p>
        </div>
    )
}

export default HomeInfoDetail