import { createBrowserRouter } from "react-router-dom"
import Home from "./views/home/Home";
import MainLayout from "./layouts/MainLayout";
import About from "./views/About";
import Contact from "./views/Contact";
import HomeInfoDetail from "./views/home/HomeInfoDetail"
export const router = createBrowserRouter([
  {
    path: "/",
    element: <MainLayout />,
    children: [
        {
            index: true,
            element: <Home />
        },
        {
            path: "home",
            element: <Home />
        },
        {
            path: "homeInfoDetail/:id",
            element: <HomeInfoDetail />
        },
        {
            path: "about",
            element: <About />
        },
        {
            path: "contact",
            element: <Contact />
        }
    ]
  }
]);